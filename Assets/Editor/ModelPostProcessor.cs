﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class ModelPostPossessor : AssetPostprocessor 
{
    public void OnPreprocessModel()
    {
        var modelImporter = assetImporter as ModelImporter;
        modelImporter.importMaterials = false;
    }
    public void OnPreprocessAnimation()
    {
        ModelImporter modelImporter = assetImporter as ModelImporter;
        if (modelImporter.clipAnimations.Length == 0 && modelImporter.defaultClipAnimations.Length != 0)
        {
            ModelImporterClipAnimation[] clipAnimations = modelImporter.defaultClipAnimations;
            for (int i = 0; i < clipAnimations.Length; i++)
            {
                if (clipAnimations[i].name.Contains("Take "))
                {
                    clipAnimations[i].takeName = Path.GetFileNameWithoutExtension(assetPath);
                    clipAnimations[i].name = clipAnimations[i].takeName;
                    clipAnimations[i].firstFrame = 0;
                }
            }
            modelImporter.clipAnimations = clipAnimations;
            modelImporter.SaveAndReimport();
        }
    }
}
