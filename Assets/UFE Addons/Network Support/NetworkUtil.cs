﻿using System;
using System.Net;
using System.Globalization;

public class NetworkUtil {

	public static IPEndPoint CreateIPEndPoint(string endPoint) {
		string[] ep = endPoint.Split(':');
		if (ep.Length != 2) {
			throw new FormatException ("Invalid endpoint format");
		}

		IPAddress ip;
		if (!IPAddress.TryParse(ep[0], out ip)) {
			throw new FormatException("Invalid ip-adress");
		}

		int port;
		if(!int.TryParse(ep[1], NumberStyles.None, NumberFormatInfo.CurrentInfo, out port)) {
			throw new FormatException("Invalid port");
		}
		return new IPEndPoint(ip, port);
	}

}

