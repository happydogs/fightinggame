using UnityEngine;
using System.Collections;

public class PhysicsScript : MonoBehaviour {
	[HideInInspector]	public bool freeze;
	[HideInInspector]	public float airTime = 0;
	[HideInInspector]	public bool isGroundBouncing;
	[HideInInspector]	public bool isWallBouncing;
	[HideInInspector]	public int wallBounceTimes;
	[HideInInspector]	public bool isTakingOff;
	[HideInInspector]	public bool isLanding;
	[HideInInspector]	public int currentAirJumps;
	[HideInInspector]	public BasicMoveInfo overrideStunAnimation;

	private float moveDirection1 = 0;
    private float moveDirection2 = 0;
    private float verticalForce = 0;
	private float horizontalForce1 = 0;
    private float horizontalForce2 = 0;
    //added for z-axis movement
    private float zAxisForce1 = 0;
    private float zAxisForce2 = 0;
    private float verticalTotalForce = 0;
    public bool overrideAirAnimation;
	private int groundLayer;
	private int groundMask;
	private int groundBounceTimes;
    private float appliedGravity;

    //my additions
    public Vector3 stageCentre = Vector3.zero;
    public float radius = UFE.config.selectedStage.ringRadius;

    private ControlsScript myControlsScript;
	private MoveSetScript myMoveSetScript;
    //private HitBoxesScript myHitBoxesScript;
	private GameObject character;

	public void Start(){
		/*Plane groundPlane = (Plane) GameObject.FindObjectOfType(typeof(Plane));
		if (groundPlane == null) Debug.LogError("Plane not found. Please add a plane mesh to your stage prefab!");*/

		groundLayer = LayerMask.NameToLayer("Ground");
  		groundMask = 1 << groundLayer;
		myControlsScript = GetComponent<ControlsScript>();
		character = myControlsScript.character;
		myMoveSetScript = character.GetComponent<MoveSetScript>();
        //myHitBoxesScript = character.GetComponent<HitBoxesScript>();
		appliedGravity = myControlsScript.myInfo.physics.weight * UFE.config.gravity;
	}
	
	public void Move(int moveDir, float direction, Camera cam){
		if (!IsGrounded()) return;
		if (freeze) return;
		if (isTakingOff) return;
        if (isLanding) return;

        if (UFE.config.inputOptions.forceDigitalInput) direction = direction < 0? -1: 1;


        if (moveDir == 1 || moveDir == -1)
        {
            Vector3 controlDir = new Vector3(myControlsScript.myInfo.physics.moveForwardSpeed * direction, 0, 0);
            Vector3 worldcontrolDir = cam.transform.TransformDirection(controlDir);
            moveDirection1 = direction;
            myControlsScript.currentSubState = SubStates.MovingForward;
            myControlsScript.horizontalForce1 = horizontalForce1 = worldcontrolDir.x;
            myControlsScript.zAxisForce1 = zAxisForce1 = worldcontrolDir.z;
            //myControlsScript.zAxisForce = zAxisForce = 0;
        }

        if (moveDir == 2 || moveDir == -2)
        {
            Vector3 controlDir = new Vector3(0, 0, myControlsScript.myInfo.physics.moveForwardSpeed * direction);
            Vector3 worldcontrolDir = cam.transform.TransformDirection(controlDir);
            moveDirection2 = direction;
            myControlsScript.currentSubState = SubStates.MovingForward;
            myControlsScript.horizontalForce2 = horizontalForce2 = worldcontrolDir.x;
            myControlsScript.zAxisForce2 = zAxisForce2 = worldcontrolDir.z;
            //myControlsScript.horizontalForce = horizontalForce = 0;
        }

        if (myControlsScript.currentMove != null)
        {
            myControlsScript.horizontalForce1 = horizontalForce1 = 0;
            myControlsScript.horizontalForce2 = horizontalForce2 = 0;
            myControlsScript.zAxisForce1 = zAxisForce1 = 0;
            myControlsScript.zAxisForce2 = zAxisForce2 = 0;
        }
    }
	
	public void Jump(){
		if (isTakingOff && currentAirJumps > 0) return;
		if (myControlsScript.currentMove != null) return;

		isTakingOff = false;
		isLanding = false;
		myControlsScript.storedMove = null;
		myControlsScript.potentialBlock = false;

		if (myControlsScript.currentState == PossibleStates.Down) return;
		if (myControlsScript.currentSubState == SubStates.Stunned || myControlsScript.currentSubState == SubStates.Blocking) return;
		if (currentAirJumps >= myControlsScript.myInfo.physics.multiJumps) return;
		currentAirJumps ++;
		horizontalForce1 = myControlsScript.myInfo.physics.jumpDistance * moveDirection1;
		verticalForce = myControlsScript.myInfo.physics.jumpForce;
		setVerticalData(myControlsScript.myInfo.physics.jumpForce);
		ApplyForces(myControlsScript.currentMove);
	}

	public bool IsJumping(){
		return (currentAirJumps > 0);
	}
	
	public bool IsMoving(){
		return (moveDirection1 + moveDirection2 != 0);
	}

    public void ResetLanding() {
        isLanding = false;
    }

	public void ResetForces(bool resetX, bool resetY, bool resetZ){
        if (resetX) horizontalForce2 = horizontalForce1 = 0;
        if (resetY) verticalForce = 0;
        if (resetZ) zAxisForce2 = zAxisForce1 = 0;
    }
	
	public void AddForce(Vector3 push, int mirror){
        isGroundBouncing = false;
        isWallBouncing = false;
		if (!myControlsScript.myInfo.physics.cumulativeForce){
            horizontalForce2 = horizontalForce1 = 0;
			verticalForce = 0;
            //my addition
            zAxisForce2 = zAxisForce1 = 0;
        }
		if (verticalForce < 0 && push.y > 0 && UFE.config.comboOptions.resetFallingForceOnHit) verticalForce = 0;
		horizontalForce1 += push.x;
		verticalForce += push.y;
        zAxisForce1 += push.z;
        setVerticalData(verticalForce);
	}
	
	void setVerticalData(float appliedForce){
		float maxHeight = Mathf.Pow(appliedForce,2) / (appliedGravity * 2);
		maxHeight += transform.position.y;
		airTime = Mathf.Sqrt(maxHeight * 2 / appliedGravity);
		verticalTotalForce = appliedGravity * airTime;
	}

	public void ApplyNewWeight(float newWeight){
		appliedGravity = newWeight * UFE.config.gravity;
	}

	public void ResetWeight(){
		appliedGravity = myControlsScript.myInfo.physics.weight * UFE.config.gravity;
	}
	
	public float GetPossibleAirTime(float appliedForce){
		float maxHeight = Mathf.Pow(appliedForce,2) / (appliedGravity * 2);
		maxHeight += transform.position.y;
		return Mathf.Sqrt(maxHeight * 2 / appliedGravity);
	}

	public void ForceGrounded() {
		verticalForce = 0;
        horizontalForce2 = horizontalForce1 = 0;
        zAxisForce2 = zAxisForce1 = 0;
        setVerticalData(0);
		currentAirJumps = 0;
		isTakingOff = false;
        isLanding = false;
        isGroundBouncing = false;
        isWallBouncing = false;
		if (transform.position.y != 0) transform.Translate(new Vector3(0, -transform.position.y, 0));
		myControlsScript.currentState = PossibleStates.Stand;
	}
	
	public void ApplyForces() {
		ApplyForces(null);
	}

	public void ApplyForces(MoveInfo move) {
		//if (myControlsScript.debugger != null) myControlsScript.debugger.text = "";
		//if (myControlsScript.debugger != null) myControlsScript.debugger.text += "IsGrounded = " + IsGrounded() + "\n";
		//if (myControlsScript.debugger != null) myControlsScript.debugger.text += "verticalForce = " + verticalForce + "\n";
		//if (myControlsScript.debugger != null) myControlsScript.debugger.text += "verticalTotalForce = " + verticalTotalForce + "\n";
		//if (myControlsScript.debugger != null) myControlsScript.debugger.text += "normalizedJumpArc = " + myControlsScript.normalizedJumpArc + "\n";
        //if (myControlsScript.debugger != null) myControlsScript.debugger.text += "isGroundBouncing = " + isGroundBouncing + "\n";
        //if (myControlsScript.debugger != null && myControlsScript.storedMove != null) myControlsScript.debugger.text += "myControlsScript.storedMove = " + myControlsScript.storedMove.name + "\n";
        //if (myControlsScript.debugger != null) myControlsScript.debugger.text += "myControlsScript.currentHitAnimation = " + myControlsScript.currentHitAnimation + "\n";
        //if (myControlsScript.debugger != null) myControlsScript.debugger.text += "isWallBouncing = " + isWallBouncing + "\n";
        //if (myControlsScript.debugger != null) myControlsScript.debugger.text += "IsJumping = " + IsJumping() + "\n";
        //if (myControlsScript.debugger != null) myControlsScript.debugger.text += "isTakingOff = " + isTakingOff + "\n";

		if (freeze) return;

		myControlsScript.normalizedJumpArc = 1 - ((verticalForce + verticalTotalForce)/(verticalTotalForce * 2));

        float moveDirection = moveDirection1 + moveDirection2;

        float appliedFriction = (moveDirection != 0 || myControlsScript.myInfo.physics.highMovingFriction) ? 
			UFE.config.selectedStage.groundFriction : myControlsScript.myInfo.physics.friction;


		if (move != null && move.ignoreFriction) appliedFriction = 0;

		if (myControlsScript.activePullIn != null){
			transform.position = Vector3.Lerp(transform.position, 
			                                  myControlsScript.activePullIn.position, 
			                                  Time.fixedDeltaTime * myControlsScript.activePullIn.speed);

			if (myControlsScript.activePullIn.forceStand && !IsGrounded()) ForceGrounded();

			if (Vector3.Distance(myControlsScript.activePullIn.position, transform.position) <= myControlsScript.activePullIn.targetDistance || 
			    myControlsScript.currentSubState != SubStates.Stunned) {
				myControlsScript.activePullIn = null;
			}

			//if (transform.position.z != 0) transform.Translate(new Vector3(0, 0, -transform.position.z));

		}else{
			if (!IsGrounded()) {
				appliedFriction = 0;
				if (verticalForce == 0) verticalForce = -.1f;
			}

            Vector3 q = Vector3.zero;
            Vector3 d = Vector3.zero;
            float horizontalForce = horizontalForce1 + horizontalForce2;
            float zAxisForce = zAxisForce1 + zAxisForce2;
            d = Vector3.forward * zAxisForce * Time.deltaTime;
            q = Vector3.right * horizontalForce * Time.deltaTime;
            Vector3 finalMove = (q + d);
            if (horizontalForce != 0 && !isTakingOff
              || zAxisForce != 0 && !isTakingOff)
            {
                if (horizontalForce > 0)
                {
                    horizontalForce1 -= appliedFriction * Time.fixedDeltaTime / 2;
                    horizontalForce1 = Mathf.Max(0, horizontalForce1);
                    horizontalForce2 -= appliedFriction * Time.fixedDeltaTime / 2;
                    horizontalForce2 = Mathf.Max(0, horizontalForce2);
                }
                else if (horizontalForce < 0)
                {
                    horizontalForce1 += appliedFriction * Time.fixedDeltaTime / 2;
                    horizontalForce1 = Mathf.Min(0, horizontalForce1);
                    horizontalForce2 += appliedFriction * Time.fixedDeltaTime / 2;
                    horizontalForce2 = Mathf.Min(0, horizontalForce2);
                }

                if (zAxisForce > 0)
                {
                    zAxisForce1 -= appliedFriction * Time.fixedDeltaTime / 2;
                    zAxisForce1 = Mathf.Max(0, zAxisForce1);
                    zAxisForce2 -= appliedFriction * Time.fixedDeltaTime / 2;
                    zAxisForce2 = Mathf.Max(0, zAxisForce2);
                }
                else if (zAxisForce < 0)
                {
                    zAxisForce1 += appliedFriction * Time.fixedDeltaTime / 2;
                    zAxisForce1 = Mathf.Min(0, zAxisForce1);
                    zAxisForce2 += appliedFriction * Time.fixedDeltaTime / 2;
                    zAxisForce2 = Mathf.Min(0, zAxisForce2);
                }

                transform.Translate(finalMove);

                float dst = Vector3.Distance(stageCentre, transform.position);
                if (dst > radius)
                {
                    Vector3 vect = stageCentre - transform.position;
                    vect = vect.normalized;
                    vect *= (dst - radius);
                    transform.position += vect;
                }
            }
            if (move == null || (move != null && !move.ignoreGravity))
            {
                if ((verticalForce < 0 && !IsGrounded()) || verticalForce > 0)
                {
                    verticalForce -= appliedGravity * Time.fixedDeltaTime;
                    transform.Translate(moveDirection * myControlsScript.myInfo.physics.jumpDistance * Time.fixedDeltaTime, verticalForce * Time.fixedDeltaTime, 0);
                }
                else if (verticalForce < 0
                  && IsGrounded()
                  && myControlsScript.currentSubState != SubStates.Stunned)
                {
                    verticalForce = 0;
                }
            }
        }

		/*if (myControlsScript.debugger != null) {
			myControlsScript.debugger.text = "isBouncing = " + isBouncing + "\n";
			myControlsScript.debugger.text += "controlsScript.stunTime = " + controlsScript.stunTime + "\n";
			myControlsScript.debugger.text += "Animations:\n";
			foreach(AnimationState animState in character.animation){
				if (myMoveSetScript.IsAnimationPlaying(animState.name)){
					myControlsScript.debugger.text += "<color=#003300>"+ animState.name +"</color>\n";
					myControlsScript.debugger.text += "<color=#003300>"+ animState.speed +"</color>\n";
				}
			}
		}*/
		
		/*if (UFE.normalizedCam) {
			Vector3 cameraLeftBounds = Camera.main.ViewportToWorldPoint(new Vector3(0,0,-Camera.main.transform.position.z));
			Vector3 cameraRightBounds = Camera.main.ViewportToWorldPoint(new Vector3(1,0,-Camera.main.transform.position.z));
			
			transform.position = new Vector3(
				Mathf.Clamp(transform.position.x,cameraLeftBounds.x,cameraRightBounds.x),
				transform.position.y,
				transform.position.z);
		}*/
		
		float minDist = myControlsScript.opponent.transform.position.x - UFE.config.cameraOptions.maxDistance;
		float maxDist = myControlsScript.opponent.transform.position.x + UFE.config.cameraOptions.maxDistance;
		transform.position = new Vector3(Mathf.Clamp(transform.position.x, minDist, maxDist), transform.position.y, transform.position.z);

		if (myControlsScript.currentState == PossibleStates.Down) return;

		if (IsGrounded() && myControlsScript.currentState != PossibleStates.Down){
            if (verticalTotalForce != 0) {
				if (groundBounceTimes < UFE.config.groundBounceOptions.maximumBounces 
                    && myControlsScript.currentSubState == SubStates.Stunned 
                    && UFE.config.groundBounceOptions.bounceForce != Sizes.None 
                    && verticalForce <= -UFE.config.groundBounceOptions.minimumBounceForce
                    && myControlsScript.currentHit.groundBounce) {
					//if (!UFE.config.groundBounceOptions.bounceHitBoxes) myHitBoxesScript.HideHitBoxes(true);

                    if (myControlsScript.currentHit.overrideForcesOnGroundBounce) {
                        if (myControlsScript.currentHit.resetGroundBounceHorizontalPush) horizontalForce2 = horizontalForce1 = 0;
                        if (myControlsScript.currentHit.resetGroundBounceVerticalPush) verticalForce = 0;
                        if (myControlsScript.currentHit.resetGroundBounceZAxisPush) zAxisForce2 = zAxisForce1 = 0;

                        float addedH = myControlsScript.currentHit.groundBouncePushForce.x;
                        float addedV = myControlsScript.currentHit.groundBouncePushForce.y;
                        float addedZ = myControlsScript.currentHit.groundBouncePushForce.z;

                        AddForce(new Vector3(addedH, addedV, addedZ), myControlsScript.mirror);

                    } else {
                        if (UFE.config.groundBounceOptions.bounceForce == Sizes.Small) {
                            AddForce(new Vector3(0, -verticalForce / 2.4f, 0), 1);
                        } else if (UFE.config.groundBounceOptions.bounceForce == Sizes.Medium) {
                            AddForce(new Vector3(0, -verticalForce / 1.8f, 0), 1);
                        } else if (UFE.config.groundBounceOptions.bounceForce == Sizes.High) {
                            AddForce(new Vector3(0, -verticalForce / 1.2f, 0), 1);
                        }
                    }

					groundBounceTimes ++;

                    if (!isGroundBouncing) {
                        myControlsScript.stunTime += airTime + UFE.config.knockDownOptions.air.knockedOutTime;

                        if (myMoveSetScript.basicMoves.groundBounce.clip1 != null) {
                            myControlsScript.currentHitAnimation = myMoveSetScript.basicMoves.groundBounce.name;
                            myMoveSetScript.PlayBasicMove(myMoveSetScript.basicMoves.groundBounce);
                        }
						//Debug.LogError("Ground Bounce animation not found! Make sure you have it set on Character -> Basic Moves -> Ground Bounce");

                        //myHitBoxesScript.HideHitBoxes(UFE.config.groundBounceOptions.bounceHitBoxes);

						if (UFE.config.groundBounceOptions.bouncePrefab != null) {
							GameObject pTemp = (GameObject) Instantiate(UFE.config.groundBounceOptions.bouncePrefab);
							pTemp.transform.parent = transform;
							pTemp.transform.localPosition = Vector3.zero;
							Destroy(pTemp, UFE.config.groundBounceOptions.bounceKillTime);
						}
						if (UFE.config.groundBounceOptions.shakeCamOnBounce){
							myControlsScript.shakeDensity = UFE.config.groundBounceOptions.shakeDensity;
						}
						UFE.PlaySound(UFE.config.groundBounceOptions.bounceSound);
						isGroundBouncing = true;
					}
					return;
				}
				verticalTotalForce = 0;
				airTime = 0;
				myMoveSetScript.totalAirMoves = 0;
                currentAirJumps = 0;

                BasicMoveInfo airAnimation = null;
                string downAnimation = "";
                
                isGroundBouncing = false;
				groundBounceTimes = 0;

                float animationSpeed = 0;
                float delayTime = 0;
				if (myControlsScript.currentMove != null && myControlsScript.currentMove.hitAnimationOverride) return;
                if (myControlsScript.currentSubState == SubStates.Stunned) {

                    if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.airRecovery.name)) {
                        myControlsScript.stunTime = 0;
                        myControlsScript.currentState = PossibleStates.Stand;

                    } else {
                        myControlsScript.stunTime = UFE.config.knockDownOptions.air.knockedOutTime + UFE.config.knockDownOptions.air.standUpTime;

                        // Hit Clips
                        if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.getHitKnockBack.name)
                             && myMoveSetScript.basicMoves.getHitKnockBack.clip2 != null) {

                            airAnimation = myMoveSetScript.basicMoves.getHitKnockBack;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);

                        } else if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.getHitHighKnockdown.name)
                             && myMoveSetScript.basicMoves.getHitHighKnockdown.clip2 != null) {

                            airAnimation = myMoveSetScript.basicMoves.getHitHighKnockdown;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);
                            myControlsScript.stunTime = UFE.config.knockDownOptions.high.knockedOutTime + UFE.config.knockDownOptions.high.standUpTime;

                        } else if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.getHitMidKnockdown.name)
                             && myMoveSetScript.basicMoves.getHitMidKnockdown.clip2 != null) {

                            airAnimation = myMoveSetScript.basicMoves.getHitMidKnockdown;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);
                            myControlsScript.stunTime = UFE.config.knockDownOptions.highLow.knockedOutTime + UFE.config.knockDownOptions.highLow.standUpTime;

                        } else if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.getHitSweep.name)
                             && myMoveSetScript.basicMoves.getHitSweep.clip2 != null) {
                            airAnimation = myMoveSetScript.basicMoves.getHitSweep;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);
                            myControlsScript.stunTime = UFE.config.knockDownOptions.sweep.knockedOutTime + UFE.config.knockDownOptions.sweep.standUpTime;

                        } else if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.getHitCrumple.name)
                             && myMoveSetScript.basicMoves.getHitCrumple.clip2 != null) {

                            airAnimation = myMoveSetScript.basicMoves.getHitCrumple;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);
                            //myControlsScript.stunTime = UFE.config.knockDownOptions.crumple.knockedOutTime + UFE.config.knockDownOptions.crumple.standUpTime;

                            // Stage Clips
                        } else if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.standingWallBounceKnockdown.name)
                             && myMoveSetScript.basicMoves.standingWallBounceKnockdown.clip2 != null) {

                            airAnimation = myMoveSetScript.basicMoves.standingWallBounceKnockdown;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);
                            myControlsScript.stunTime = UFE.config.knockDownOptions.wallbounce.knockedOutTime + UFE.config.knockDownOptions.wallbounce.standUpTime;

                        } else if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.airWallBounce.name)
                             && myMoveSetScript.basicMoves.airWallBounce.clip2 != null) {

                            airAnimation = myMoveSetScript.basicMoves.airWallBounce;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);
                            myControlsScript.stunTime = UFE.config.knockDownOptions.wallbounce.knockedOutTime + UFE.config.knockDownOptions.wallbounce.standUpTime;

                            // Fall Clips
                        } else if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.fallingFromAirHit.name)
                            && myMoveSetScript.basicMoves.fallingFromAirHit.clip2 != null) {

                            airAnimation = myMoveSetScript.basicMoves.fallingFromAirHit;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);

                        } else if (myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.fallingFromGroundBounce.name)
                            && myMoveSetScript.basicMoves.fallingFromGroundBounce.clip2 != null) {

                            airAnimation = myMoveSetScript.basicMoves.fallingFromGroundBounce;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 2);

                        } else {
                            if (myMoveSetScript.basicMoves.fallDown.clip1 == null)
                                Debug.LogError("Fall Down From Air Hit animation not found! Make sure you have it set on Character -> Basic Moves -> Fall Down From Air Hit");

                            airAnimation = myMoveSetScript.basicMoves.fallDown;
                            downAnimation = myMoveSetScript.GetAnimationString(airAnimation, 1);
                        }

                        /*if (airAnimation.autoSpeed) {
                            animationSpeed = myMoveSetScript.GetAnimationLengh(airAnimation.name) / myControlsScript.stunTime;
                        }*/

                        myControlsScript.currentState = PossibleStates.Down;
                        //if (!UFE.config.knockDownOptions.air.standUpHitBoxes) myHitBoxesScript.HideHitBoxes(true);

                    }

				} else if (myControlsScript.currentState != PossibleStates.Stand){
                    if (myMoveSetScript.basicMoves.landing.clip1 != null
                        && (myControlsScript.currentMove == null ||
                        (myControlsScript.currentMove != null && myControlsScript.currentMove.cancelMoveWheLanding))){

                        myControlsScript.isAirRecovering = false;
						airAnimation = myMoveSetScript.basicMoves.landing;
						moveDirection2 = moveDirection1 = 0;
                        isLanding = true;
						myControlsScript.KillCurrentMove();
                        delayTime = (float)myControlsScript.myInfo.physics.landingDelay / UFE.config.fps;
                        UFE.DelaySynchronizedAction(ResetLanding, delayTime);

                        if (airAnimation.autoSpeed) {
                            animationSpeed = myMoveSetScript.GetAnimationLength(airAnimation.name) / delayTime;
                        }
					}

					if (myControlsScript.currentState != PossibleStates.Crouch) myControlsScript.currentState = PossibleStates.Stand;

				}

				if (airAnimation != null) {
                    if (downAnimation != "") {
                        myMoveSetScript.PlayBasicMove(airAnimation, downAnimation);
                    } else {
                        myMoveSetScript.PlayBasicMove(airAnimation);
                    }

                    if (animationSpeed != 0) {
                        myMoveSetScript.SetAnimationSpeed(airAnimation.name, animationSpeed);
                    }
				}
			}

            moveDirection = moveDirection1 + moveDirection2;

			if (myControlsScript.currentSubState != SubStates.Stunned 
                && !myControlsScript.isBlocking && !myControlsScript.blockStunned 
                && move == null 
                && !isTakingOff 
                && !isLanding 
                && myControlsScript.currentState == PossibleStates.Stand){
                    if (moveDirection != 0)
                    {
                        if (myMoveSetScript.basicMoves.moveForward.clip1 == null)
                            Debug.LogError("Move Forward animation not found! Make sure you have it set on Character -> Basic Moves -> Move Forward");
                        if (!myMoveSetScript.IsAnimationPlaying(myMoveSetScript.basicMoves.moveForward.name))
                        {
                            myMoveSetScript.PlayBasicMove(myMoveSetScript.basicMoves.moveForward);
                        }
                    }
			}
        } else if (verticalForce > 0 || !IsGrounded()) {
			if (move != null && myControlsScript.currentState == PossibleStates.Stand)
				myControlsScript.currentState = PossibleStates.StraightJump;
			if (move == null && verticalForce/verticalTotalForce > 0 && verticalForce/verticalTotalForce <= 1) {
				if (isGroundBouncing) return;

                if (moveDirection == 0)
                {
					myControlsScript.currentState = PossibleStates.StraightJump;
				}else{
                    if (moveDirection > 0 && myControlsScript.mirror == -1 ||
                        moveDirection < 0 && myControlsScript.mirror == 1)
                    {
						myControlsScript.currentState = PossibleStates.ForwardJump;
					}

					if (moveDirection > 0 && myControlsScript.mirror == 1||
					    moveDirection < 0 && myControlsScript.mirror == -1) {
						myControlsScript.currentState = PossibleStates.BackJump;
					}
				}

                BasicMoveInfo airAnimation = myMoveSetScript.basicMoves.jumpStraight;
				if (myControlsScript.currentSubState == SubStates.Stunned){
                    if (isWallBouncing && myMoveSetScript.basicMoves.airWallBounce.clip1 != null) {
                        airAnimation = myMoveSetScript.basicMoves.airWallBounce;

                    } else if (myMoveSetScript.basicMoves.getHitKnockBack.clip1 != null &&
                        Mathf.Abs(horizontalForce1 + horizontalForce2) > UFE.config.comboOptions.knockBackMinForce && 
					    UFE.config.comboOptions.knockBackMinForce > 0){
						airAnimation = myMoveSetScript.basicMoves.getHitKnockBack;
                        airTime *= 2;

					} else {
						if (myMoveSetScript.basicMoves.getHitAir.clip1 == null)
							Debug.LogError("Get Hit Air animation not found! Make sure you have it set on Character -> Basic Moves -> Get Hit Air");

                        airAnimation = myMoveSetScript.basicMoves.getHitAir;
                    }
                    if (overrideStunAnimation != null) airAnimation = overrideStunAnimation;

                } else if (myControlsScript.isAirRecovering 
                    && (myMoveSetScript.basicMoves.airRecovery.clip1 != null)) {
						airAnimation = myMoveSetScript.basicMoves.airRecovery;

				} else {
					if (myMoveSetScript.basicMoves.jumpForward.clip1 != null && myControlsScript.currentState == PossibleStates.ForwardJump) {
						airAnimation = myMoveSetScript.basicMoves.jumpForward;
					} else if (myMoveSetScript.basicMoves.jumpBack.clip1 != null && myControlsScript.currentState == PossibleStates.BackJump) {
						airAnimation = myMoveSetScript.basicMoves.jumpBack;
					} else {
						if (myMoveSetScript.basicMoves.jumpStraight.clip1 == null)
							Debug.LogError("Jump animation not found! Make sure you have it set on Character -> Basic Moves -> Jump Straight");

						airAnimation = myMoveSetScript.basicMoves.jumpStraight;
					}
				}

                if (!overrideAirAnimation && !myMoveSetScript.IsAnimationPlaying(airAnimation.name)) {
                    myMoveSetScript.PlayBasicMove(airAnimation);
                    //if (airAnimation.invincible) myHitBoxesScript.HideHitBoxes(true);

                    if (airAnimation.autoSpeed)
                        myMoveSetScript.SetAnimationNormalizedSpeed(airAnimation.name, (myMoveSetScript.GetAnimationLength(airAnimation.name) / airTime));
                    
                    //if (airAnimation.autoSpeed || myControlsScript.currentSubState == SubStates.Stunned) {
                    //myMoveSetScript.SetAnimationNormalizedSpeed(airAnimation.name, (myMoveSetScript.GetAnimationLengh(airAnimation.name) / airTime));
				}

            } else if (move == null && verticalForce / verticalTotalForce <= 0) {

                BasicMoveInfo airAnimation = myMoveSetScript.basicMoves.fallStraight;
                if (isGroundBouncing && myMoveSetScript.basicMoves.fallingFromGroundBounce.clip1 != null) {
                    airAnimation = myMoveSetScript.basicMoves.fallingFromGroundBounce;
                    //if (myMoveSetScript.basicMoves.fallingFromGroundBounce.invincible) myHitBoxesScript.HideHitBoxes(true);

                } else if (isWallBouncing && myMoveSetScript.basicMoves.airWallBounce.clip1 != null) {
                    airAnimation = myMoveSetScript.basicMoves.airWallBounce;
                    //if (myMoveSetScript.basicMoves.airWallBounce.invincible) myHitBoxesScript.HideHitBoxes(true);

				} else {
					if (myControlsScript.currentSubState == SubStates.Stunned){
						if (myMoveSetScript.basicMoves.getHitKnockBack.clip1 != null &&
                            Mathf.Abs(horizontalForce1 + horizontalForce2) > UFE.config.comboOptions.knockBackMinForce && 
						    UFE.config.comboOptions.knockBackMinForce > 0){
							airAnimation = myMoveSetScript.basicMoves.getHitKnockBack;

                        } else {
                            airAnimation = myMoveSetScript.basicMoves.getHitAir;
                            if (myMoveSetScript.basicMoves.fallingFromAirHit.clip1 != null) {
                                airAnimation = myMoveSetScript.basicMoves.fallingFromAirHit;

                            } else if (myMoveSetScript.basicMoves.getHitAir.clip1 == null) {
                                Debug.LogError("Air Juggle animation not found! Make sure you have it set on Character -> Basic Moves -> Air Juggle");
                            }
                        }
                        if (overrideStunAnimation != null) airAnimation = overrideStunAnimation;

                    } else if (myControlsScript.isAirRecovering 
                        && (myMoveSetScript.basicMoves.airRecovery.clip1 != null)) {
                        airAnimation = myMoveSetScript.basicMoves.airRecovery;

					} else {
						if (myMoveSetScript.basicMoves.fallForward.clip1 != null && myControlsScript.currentState == PossibleStates.ForwardJump) {
							airAnimation = myMoveSetScript.basicMoves.fallForward;
						} else if (myMoveSetScript.basicMoves.fallBack.clip1 != null && myControlsScript.currentState == PossibleStates.BackJump) {
							airAnimation = myMoveSetScript.basicMoves.fallBack;
						} else {
							if (myMoveSetScript.basicMoves.fallStraight.clip1 == null)
								Debug.LogError("Fall animation not found! Make sure you have it set on Character -> Basic Moves -> Fall Straight");
							
							airAnimation = myMoveSetScript.basicMoves.fallStraight;
						}
					}
				}

                if (!overrideAirAnimation && !myMoveSetScript.IsAnimationPlaying(airAnimation.name)) {
                    myMoveSetScript.PlayBasicMove(airAnimation);

                    if (airAnimation.autoSpeed) {
                        //myMoveSetScript.SetAnimationNormalizedSpeed(airAnimation.name, (myMoveSetScript.GetAnimationLengh(airAnimation.name) / (airTime * 2)));
                        myMoveSetScript.SetAnimationNormalizedSpeed(airAnimation.name, (myMoveSetScript.GetAnimationLength(airAnimation.name) / airTime));
                    }
				}
			}
		}
        if (horizontalForce1 + horizontalForce2 == 0 && verticalForce == 0 && zAxisForce1 + zAxisForce2 == 0) 
            moveDirection2 = moveDirection1 = 0;
	}

	public bool IsGrounded() {
		if (Physics.RaycastAll(transform.position + new Vector3(0, 2f, 0), Vector3.down, 2.02f, groundMask).Length > 0) {
			if (transform.position.y != 0) transform.Translate(new Vector3(0, -transform.position.y, 0));
			return true;
		}
		return false;
	}
}
