using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public GameObject playerLight;
	[HideInInspector] public bool killCamMove;
	[HideInInspector] public bool cinematicFreeze;
	
	private Vector3 targetPosition;
    private Vector3 currentLookAtPosition;
	private Quaternion targetRotation;
    private float targetFieldOfView;
    private float movementSpeed;
    private float rotationSpeed;
    private float freeCameraSpeed;
	
	private Transform player1;
	private Transform player2;
	private string lastOwner;

    //private Vector3 cameraStartingPos;
    //private float standardZoom;
    private float standardDistance;
	private float standardGroundHeight;

    public Camera opponentMainCamera;
    private Vector3 opponentCurrentLookAtPosition;
    //private Quaternion standardRotation;
    //private float fieldOfView;


    void Start(){
		playerLight = GameObject.Find("Player Light");

        int player = UFE.GetLocalPlayer();
        if(player == 2)
        {
            player1 = GameObject.Find("Player2").transform;
            player2 = GameObject.Find("Player1").transform;
        }
        else
        {
            player1 = GameObject.Find("Player1").transform;
            player2 = GameObject.Find("Player2").transform;
        }

        GameObject opponentMainCameraObject = GameObject.Find("OpponentCamera");
        opponentMainCamera = opponentMainCameraObject.GetComponent<Camera>();

        ResetCam();
        ResetOpponentCam();
        //standardZoom = UFE.config.cameraOptions.initialDistance.z;
        standardDistance = Vector3.Distance(player1.position, player2.position);
        movementSpeed = UFE.config.cameraOptions.movementSpeed;
        rotationSpeed = UFE.config.cameraOptions.rotationSpeed;
        UFE.freeCamera = false;

	}

	public void ResetCam(){
        Camera.main.transform.localPosition = UFE.config.cameraOptions.initialDistance;
		Camera.main.transform.position = UFE.config.cameraOptions.initialDistance;
		Camera.main.transform.localRotation = Quaternion.Euler(UFE.config.cameraOptions.initialRotation);
		Camera.main.fieldOfView = UFE.config.cameraOptions.initialFieldOfView;
        //standardGroundHeight = Camera.main.transform.position.y;
    }

    public void ResetOpponentCam()
    {
        opponentMainCamera.transform.localPosition = UFE.config.cameraOptions.initialDistance;
        opponentMainCamera.transform.position = UFE.config.cameraOptions.initialDistance;
        opponentMainCamera.transform.localRotation = Quaternion.Euler(UFE.config.cameraOptions.initialRotation);

        opponentMainCamera.fieldOfView = UFE.config.cameraOptions.initialFieldOfView;
        //standardGroundHeight = Camera.main.transform.position.y;
    }

    public Vector3 LerpByDistance(Vector3 A, Vector3 B, float speed){
		Vector3 P = speed * Time.fixedDeltaTime * Vector3.Normalize(B - A) + A;
		return P;
	}

	public void DoFixedUpdate() {
		if (killCamMove) return;
		if (UFE.freeCamera) {
			Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, targetFieldOfView, Time.fixedDeltaTime * freeCameraSpeed * 1.8f);
			Camera.main.transform.localPosition = Vector3.Lerp(Camera.main.transform.localPosition, targetPosition, Time.fixedDeltaTime * freeCameraSpeed * 1.8f);
			Camera.main.transform.localRotation = Quaternion.Slerp(Camera.main.transform.localRotation, targetRotation, Time.fixedDeltaTime * freeCameraSpeed * 1.8f);

		}else{
            CalculateCamera(Camera.main, player1, player2, false);
            CalculateCamera(opponentMainCamera, player2, player1, true);
        }
	}

    private void CalculateCamera(Camera cam, Transform player1trans, Transform player2trans, bool opponent)
    {
        float initialAngle = UFE.config.cameraOptions.initialAngle;
        float maxAngle = UFE.config.cameraOptions.maxAngle;
        float minAngle = UFE.config.cameraOptions.minAngle;
        float lockDistance = UFE.config.cameraOptions.lockDistance;
        float initialHeight = UFE.config.cameraOptions.initialHeight;
        float maxHeight = UFE.config.cameraOptions.maxHeight;
        float minHeight = UFE.config.cameraOptions.minHeight;
        float maxDistance = UFE.config.cameraOptions.maxDistance;
        float initialDistanceFromPlayers = UFE.config.cameraOptions.initialDistanceFromPlayers;
        float maxDistanceFromPlayers = UFE.config.cameraOptions.maxDistanceFromPlayers;
        float minDistanceFromPlayers = UFE.config.cameraOptions.minDistanceFromPlayers;
        float initialLookAtOffsetScale = UFE.config.cameraOptions.initialLookAtOffsetScale;

        Vector3 newPosition;

        Vector3 midPoint = (player1trans.position + player2trans.position) / 2;
        Vector3 playerDir = player1trans.position - player2trans.position;
        float initialSpawnDistance = Mathf.Abs(UFE.config.roundOptions.p1XPosition) + Mathf.Abs(UFE.config.roundOptions.p2XPosition);

        if (playerDir.magnitude < lockDistance)
        {
            Vector3 cameraOffDir = Quaternion.Euler(0, -maxAngle, 0) * playerDir;
            newPosition = midPoint + cameraOffDir.normalized * minDistanceFromPlayers;
            newPosition.y = minHeight;
        }
        else
        {
            if (playerDir.magnitude < initialSpawnDistance)
            {
                float t = (playerDir.magnitude - lockDistance) / (initialSpawnDistance - lockDistance);
                float angle = Mathf.Lerp(maxAngle, initialAngle, t);
                float height = Mathf.Lerp(minHeight, initialHeight, t);
                Vector3 cameraOffDir = Quaternion.Euler(0, -angle, 0) * playerDir;
                newPosition = midPoint + cameraOffDir.normalized * Mathf.Lerp(minDistanceFromPlayers, initialDistanceFromPlayers, t);
                newPosition.y = height;
            }
            else
            {
                float t = (playerDir.magnitude - initialSpawnDistance) / (maxDistance - initialSpawnDistance);
                float angle = Mathf.Lerp(initialAngle, minAngle, t);
                float height = Mathf.Lerp(initialHeight, maxHeight, t);
                Vector3 cameraOffDir = Quaternion.Euler(0, -angle, 0) * playerDir;
                newPosition = midPoint + cameraOffDir.normalized * Mathf.Lerp(initialDistanceFromPlayers, maxDistanceFromPlayers, t);
                newPosition.y = height;
            }
        }

        if (UFE.config.cameraOptions.followJumpingCharacter)
            newPosition.y += Mathf.Abs(player1trans.position.y - player2trans.position.y) / 2;

        //newPosition.x = Mathf.Clamp(newPosition.x, 
        //	UFE.config.selectedStage.leftBoundary + 8, 
        //	UFE.config.selectedStage.rightBoundary - 8);

        //         newPosition.z = UFE.config.cameraOptions.initialDistance.z - Vector3.Distance(player1.position, player2.position) + standardDistance;
        //newPosition.z = Mathf.Clamp(newPosition.z, -UFE.config.cameraOptions.maxZoom, -UFE.config.cameraOptions.minZoom);

        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, UFE.config.cameraOptions.initialFieldOfView, Time.fixedDeltaTime * movementSpeed);
        cam.transform.position = Vector3.Lerp(cam.transform.position, newPosition, Time.fixedDeltaTime * movementSpeed);
        cam.transform.localRotation = Quaternion.Slerp(cam.transform.localRotation, Quaternion.Euler(UFE.config.cameraOptions.initialRotation), Time.fixedDeltaTime * UFE.config.cameraOptions.movementSpeed);

        //if (Camera.main.transform.localRotation == Quaternion.Euler(UFE.config.cameraOptions.initialRotation))
        //	UFE.normalizedCam = true;

        if (playerLight != null) playerLight.GetComponent<Light>().enabled = false;

        if (UFE.config.cameraOptions.enableLookAt)
        {
            //Vector3 lookAtPosition = ((player1.position + player2.position)/2);
            //lookAtPosition.y += UFE.config.cameraOptions.heightOffSet;

            float lookAtOffsetScale = Mathf.Lerp(0.5f, initialLookAtOffsetScale,
                Mathf.Clamp((playerDir.magnitude - lockDistance) / (initialSpawnDistance - lockDistance), 0, 1));

            if(!opponent)
            {
                currentLookAtPosition = Vector3.Lerp(currentLookAtPosition,
    lookAtOffsetScale * player1.position + (1 - lookAtOffsetScale) * player2.position + UFE.config.cameraOptions.lookAtOffSet,
    Time.fixedDeltaTime * rotationSpeed);

                cam.transform.LookAt(currentLookAtPosition, Vector3.up);
            }
            else
            {
                opponentCurrentLookAtPosition = Vector3.Lerp(opponentCurrentLookAtPosition,
lookAtOffsetScale * player1.position + (1 - lookAtOffsetScale) * player2.position + UFE.config.cameraOptions.lookAtOffSet,
Time.fixedDeltaTime * rotationSpeed);

                cam.transform.LookAt(opponentCurrentLookAtPosition, Vector3.up);
            }

        }
    }

	public void MoveCameraToLocation(Vector3 targetPos, Vector3 targetRot, float targetFOV, float speed, string owner){
		targetFieldOfView = targetFOV;
		targetPosition = targetPos;
		targetRotation = Quaternion.Euler(targetRot);
		freeCameraSpeed = speed;
		UFE.freeCamera = true;
		UFE.normalizedCam = false;
		lastOwner = owner;
		if (playerLight != null) playerLight.GetComponent<Light>().enabled = true;
	}
	
	public void DisableCam(){
		Camera.main.enabled = false;
	}

	public void ReleaseCam(){
		Camera.main.enabled = true;
		cinematicFreeze = false;
		UFE.freeCamera = false;
		lastOwner = "";
	}

    public void OverrideSpeed(float newMovement, float newRotation) {
        movementSpeed = newMovement;
        rotationSpeed = newRotation;
    }

    public void RestoreSpeed() {
        movementSpeed = UFE.config.cameraOptions.movementSpeed;
        rotationSpeed = UFE.config.cameraOptions.rotationSpeed;
    }

	public void SetCameraOwner(string owner){
		lastOwner = owner;
	}

	public string GetCameraOwner(){
		return lastOwner;
	}

	public Vector3 GetRelativePosition(Transform origin, Vector3 position) {
		Vector3 distance = position - origin.position;
		Vector3 relativePosition = Vector3.zero;
		relativePosition.x = Vector3.Dot(distance, origin.right.normalized);
		relativePosition.y = Vector3.Dot(distance, origin.up.normalized);
		relativePosition.z = Vector3.Dot(distance, origin.forward.normalized);
		
		return relativePosition;
	}

    void OnDrawGizmos() {
        Vector3 cameraLeftBounds = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z));
        Vector3 cameraRightBounds = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, -Camera.main.transform.position.z));

        //cameraLeftBounds.x = Camera.main.transform.position.x - (UFE.config.cameraOptions.maxDistance / 2);
        //cameraRightBounds.x = Camera.main.transform.position.x + (UFE.config.cameraOptions.maxDistance / 2);

        Gizmos.DrawLine(cameraLeftBounds, cameraLeftBounds + new Vector3(0, 15, 0));
        Gizmos.DrawLine(cameraRightBounds, cameraRightBounds + new Vector3(0, 15, 0));
        //Gizmos.DrawWireSphere(cameraRightBounds, 1);
    }
}
