﻿using UnityEngine;
using RakNet;
using System;

//
// 摘要:
//     ///
//     Describes the status of the network interface peer type as returned by Network.peerType.
//     ///
public enum RakNetNetworkPeerType
{

    //
    // 摘要:
    //     ///
    //     Running as server.
    //     ///
    Server = 0,
    //
    // 摘要:
    //     ///
    //     Running as client.
    //     ///
    Client = 1

}

public enum RakNetNetworkState
{
    //
    // 摘要:
    //     ///
    //     No client connection running. Server not initialized.
    //     ///
    Disconnected = 0,
    //
    // 摘要:
    //     ///
    //     Attempting to connect to a server.
    //     ///
    Connecting = 1
}

public class RakNetNetworkMultiplayerAPI : MultiplayerAPI
{
    #region protected instance fields
    protected int _connectionId = -1;
    protected int _hostId = -1;
    protected NetworkPeerType? _previousPeerType = null;
    protected int _unreliableChannel = -1;
    protected RakPeerInterface _peer;
    RakNetNetworkPeerType _type = RakNetNetworkPeerType.Client;
    RakNetNetworkState _state = RakNetNetworkState.Disconnected;
    int _connectionLength = 0;
    AddressOrGUID _peerAddress;
    #endregion

    #region public override properties
    public override int Connections
    {
        get
        {
            return _connectionLength;
        }
    }

    public override NetworkPlayer NetworkPlayer
    {
        get
        {
            return Network.player;
        }
    }

    public override float SendRate
    {
        get
        {
            return Network.sendRate;
        }
        set
        {
            Network.sendRate = value;
        }
    }
    #endregion

    #region public override methods
    public override void Connect(string ip, int port)
    {
        //NetworkConnectionError error = Network.Connect(ip, port);

        //if (error != NetworkConnectionError.NoError){
        //	this.RaiseOnServerConnectionError(error);
        //}
        _peer = RakPeerInterface.GetInstance();
        _type = RakNetNetworkPeerType.Client;
        SocketDescriptor socket = new SocketDescriptor();
        _peer.Startup(1, socket, 1);
        _peer.Connect(ip, (ushort)port, null, 0);
    }

    public override bool Disconnect()
    {
        _peer.Dispose();
        RakPeerInterface.DestroyInstance(_peer);
        return true;
    }

    public override void FindLanGames()
    {
        this.RaiseOnLanGamesDiscoveryError(NetworkConnectionError.ConnectionFailed);
    }

    public override NetworkState GetConnectionState()
    {
        if (_state == RakNetNetworkState.Disconnected)
            return NetworkState.Disconnected;
        else
        {
            if (_type == RakNetNetworkPeerType.Client)
                return NetworkState.Client;
            else
                return NetworkState.Server;
        }
    }

    public override void StartServer(int maxConnections, int listenPort, bool useNat)
    {
        _peer = RakPeerInterface.GetInstance();
        _type = RakNetNetworkPeerType.Server;
        SocketDescriptor socket = new SocketDescriptor((ushort)listenPort, "0.0.0.0");

        _peer.Startup((uint)maxConnections, socket, 1);

        _peer.SetMaximumIncomingConnections((ushort)maxConnections);

        OnServerInitialized();
    }

    public override bool StopServer()
    {
        //_peer.Dispose();
        RakPeerInterface.DestroyInstance(_peer);

        return true;
    }
    #endregion

    #region protected override methods
    public override bool SendNetworkMessage(byte[] bytes)
    {
        if (_peer != null)
        {
            RakNet.BitStream stream = new RakNet.BitStream();
            stream.Write((byte)240);
            for (int i = 0; i < bytes.Length; i++)
                stream.Write(bytes[i]);
            //stream.WriteBits(bytes, length, false);
            _peer.Send(stream, PacketPriority.MEDIUM_PRIORITY, PacketReliability.RELIABLE_ORDERED, (char)0, _peerAddress, false);
        }
        return true;
    }
    #endregion

    #region protected instance methods
    [RPC]
    public virtual void Legacy(byte[] bytes, NetworkMessageInfo msgInfo)
    {
        this.RaiseOnMessageReceived(bytes, msgInfo);
    }
    #endregion

    #region protected MonoBehaviour methods
    protected virtual void OnConnectedToServer()
    {
        this.RaiseOnServerConnectionSuccess();
    }

    protected virtual void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        this.RaiseOnDisconnection(info);
    }

    protected virtual void OnPlayerConnected(NetworkPlayer player)
    {
        this.RaiseOnPlayerConnectedToServer(player);
    }

    protected virtual void OnPlayerDisconnected(NetworkPlayer player)
    {
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
        this.RaiseOnPlayerDisconnectedFromServer(player);
    }

    protected virtual void OnServerInitialized()
    {
        this.RaiseOnServerStarted();
    }
    #endregion

    #region message handling

    private void OnConnection(RakNet.Packet packet)
    {
        UnityEngine.Debug.LogFormat("transports.raknet", "{0} connected", packet.systemAddress);

        _connectionLength++;
        _state = RakNetNetworkState.Connecting;
        _peer.DeallocatePacket(packet);
        if (_type == RakNetNetworkPeerType.Client)
            OnConnectedToServer();
        else
        {
            NetworkPlayer player = new NetworkPlayer();
            OnPlayerConnected(player);
        }

    }

    private void OnDisconnection(RakNet.Packet packet, string reason)
    {
        UnityEngine.Debug.LogFormat("transports.raknet", "{0} disconnected", packet.systemAddress);

        _connectionLength--;
        _state = RakNetNetworkState.Disconnected;
        _peer.DeallocatePacket(packet);

        if (_type == RakNetNetworkPeerType.Client)
        {
            NetworkDisconnection info = new NetworkDisconnection();
            OnDisconnectedFromServer(info);
        }

        else
        {
            NetworkPlayer player = new NetworkPlayer();
            OnPlayerDisconnected(player);
        }
    }

    private void OnMessageReceivedProcess(RakNet.Packet packet)
    {
        var buffer = new byte[packet.data.Length - 1];
        for (int i = 0; i < packet.data.Length - 1; i++)
            buffer[i] = packet.data[i + 1];
        //packet.data.CopyTo(buffer,0);
        _peer.DeallocatePacket(packet);

        NetworkMessageInfo msgInfo = new NetworkMessageInfo();
        this.RaiseOnMessageReceived(buffer, msgInfo);
    }
    #endregion
    public void Run()
    {
        try
        {
            for (var packet = _peer.Receive(); packet != null; packet = _peer.Receive())
            {
                _peerAddress = packet.systemAddress;
                try
                {
                    switch (packet.data[0])
                    {
                        case (byte)DefaultMessageIDTypes.ID_CONNECTION_REQUEST_ACCEPTED:
                            UnityEngine.Debug.LogFormat("Connection request to {0} accepted.", packet.systemAddress.ToString());
                            OnConnection(packet);
                            break;
                        case (byte)DefaultMessageIDTypes.ID_NEW_INCOMING_CONNECTION:
                            UnityEngine.Debug.LogFormat("Incoming connection from {0}.", packet.systemAddress.ToString());
                            OnConnection(packet);
                            break;
                        case (byte)DefaultMessageIDTypes.ID_DISCONNECTION_NOTIFICATION:
                            UnityEngine.Debug.LogFormat("{0} disconnected.", packet.systemAddress.ToString());
                            OnDisconnection(packet, "CLIENT_DISCONNECTED");
                            break;
                        case (byte)DefaultMessageIDTypes.ID_CONNECTION_LOST:
                            UnityEngine.Debug.LogFormat("{0} lost the connection.", packet.systemAddress.ToString());
                            OnDisconnection(packet, "CONNECTION_LOST");
                            break;
                        case (byte)DefaultMessageIDTypes.ID_CONNECTION_ATTEMPT_FAILED:
                            UnityEngine.Debug.LogFormat("Connection attempt failed.");
                            OnDisconnection(packet, "Connection attempt failed.");
                            break;
                        default:
                            OnMessageReceivedProcess(packet);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.LogFormat("transports.raknet", "An error occured while handling a message", ex.ToString());
                }
            }
        }
        catch (Exception ex)
        {

            UnityEngine.Debug.LogFormat("transports.raknet", "An error occured while running the transport : {0}", ex);

        }
    }

    protected virtual void FixedUpdate()
    {
        if (_peer != null)
            Run();
    }

}
