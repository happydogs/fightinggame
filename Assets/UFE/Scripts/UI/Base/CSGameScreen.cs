﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using UnityEngine.UI;

public class CSGameScreen : UFEScreen
{
    public Toggle useInputServerIPToggle;
    public Text ipText;
    
    public virtual void GoToMainMenu(){
        UFE.StartMainMenuScreen();
    }
    
    public virtual void HostCSGame()
    {
        if (useInputServerIPToggle.isOn)
        {
            UFE.HostCSGame(ipText.text);
        }
        else
        {
            UFE.HostCSGame();
        }
    }

    public virtual void JoinCSGame(){
        if (useInputServerIPToggle.isOn)
        {
            UFE.JoinCSGame(ipText.text);
        }
        else
        {
            UFE.JoinCSGame();
        }
    }

    public virtual string GetIPv6() {
        string hostName = System.Net.Dns.GetHostName();
        IPHostEntry ipHostEntry = System.Net.Dns.GetHostEntry(hostName);
        IPAddress[] ipAddresses = ipHostEntry.AddressList;
		
        return ipAddresses[ipAddresses.Length - 1].ToString();
    }

    public virtual string GetIP() {
        return Network.player.ipAddress.ToString();
    }
}
