/*
-----------------------------------------------------------------------------
This source file is part of OGRE
(Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright (c) 2000-2009 Torus Knot Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
OgreSegment.h  -  3D Line Segment class for intersection testing in Ogre3D
Some algorithms based off code from the Wild Magic library by Dave Eberly
-----------------------------------------------------------------------------
begin                : Mon Apr 02 2007
author               : Eric Cha
email                : ericc@xenopi.com
Code Style Update	 : Apr 5, 2007
-----------------------------------------------------------------------------
*/

using UnityEngine;


class Segment
{
    // The segment is represented as P+t*D, where P is the segment origin,
    // D is a unit-length direction vector and |t| <= e.  The value e is
    // referred to as the extent of the segment.  The end points of the
    // segment are P-e*D and P+e*D.  The user must ensure that the direction
    // vector is unit-length.  The representation for a segment is analogous
    // to that for an oriented bounding box.  P is the center, D is the
    // axis direction, and e is the extent.
    // defining variables
    private Vector3 mOrigin;
    private Vector3 mEnd;
    private Vector3 mDirection;
    private float mLength;
    const float PARALLEL_TOLERANCE = 0.0001f;

    // construction
    public Segment()  // uninitialized
    {

    }
    public Segment(Vector3 middlePoint,Vector3 lineDirection, float lineLength)
    {
        mOrigin = middlePoint + lineDirection.normalized * lineLength / 2;
        mEnd = middlePoint - lineDirection.normalized * lineLength / 2;
        mDirection = mEnd - mOrigin;
        mLength = lineLength;
    }
    // functions to calculate distance to another segment
    public float Distance(Segment otherSegment)
    {
        float fSqrDist = SquaredDistance(otherSegment);
        return Mathf.Sqrt(fSqrDist);
    }
	public float SquaredDistance(Segment otherSegment)
    {
        Vector3 kDiff = mOrigin - otherSegment.mOrigin;
        float fA01 = -Vector3.Dot(mDirection,otherSegment.mDirection);
        float fB0 = Vector3.Dot(kDiff,mDirection);
        float fB1 = -Vector3.Dot(kDiff,otherSegment.mDirection);
        float fC = kDiff.sqrMagnitude;
        float fDet = Mathf.Abs((float)1.0 - fA01 * fA01);
        float fS0, fS1, fSqrDist, fExtDet0, fExtDet1, fTmpS0, fTmpS1;

        if (fDet >= PARALLEL_TOLERANCE)
        {
            // segments are not parallel
            fS0 = fA01 * fB1 - fB0;
            fS1 = fA01 * fB0 - fB1;
            fExtDet0 = mLength * fDet;
            fExtDet1 = otherSegment.mLength * fDet;

            if (fS0 >= -fExtDet0)
            {
                if (fS0 <= fExtDet0)
                {
                    if (fS1 >= -fExtDet1)
                    {
                        if (fS1 <= fExtDet1)  // region 0 (interior)
                        {
                            // minimum at two interior points of 3D lines
                            float fInvDet = ((float)1.0) / fDet;
                            fS0 *= fInvDet;
                            fS1 *= fInvDet;
                            fSqrDist = fS0 * (fS0 + fA01 * fS1 + ((float)2.0) * fB0) +
                                fS1 * (fA01 * fS0 + fS1 + ((float)2.0) * fB1) + fC;
                        }
                        else  // region 3 (side)
                        {
                            fS1 = otherSegment.mLength;
                            fTmpS0 = -(fA01 * fS1 + fB0);
                            if (fTmpS0 < -mLength)
                            {
                                fS0 = -mLength;
                                fSqrDist = fS0 * (fS0 - ((float)2.0) * fTmpS0) +
                                    fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                            }
                            else if (fTmpS0 <= mLength)
                            {
                                fS0 = fTmpS0;
                                fSqrDist = -fS0 * fS0 + fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                            }
                            else
                            {
                                fS0 = mLength;
                                fSqrDist = fS0 * (fS0 - ((float)2.0) * fTmpS0) +
                                    fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                            }
                        }
                    }
                    else  // region 7 (side)
                    {
                        fS1 = -otherSegment.mLength;
                        fTmpS0 = -(fA01 * fS1 + fB0);
                        if (fTmpS0 < -mLength)
                        {
                            fS0 = -mLength;
                            fSqrDist = fS0 * (fS0 - ((float)2.0) * fTmpS0) +
                                fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                        }
                        else if (fTmpS0 <= mLength)
                        {
                            fS0 = fTmpS0;
                            fSqrDist = -fS0 * fS0 + fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                        }
                        else
                        {
                            fS0 = mLength;
                            fSqrDist = fS0 * (fS0 - ((float)2.0) * fTmpS0) +
                                fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                        }
                    }
                }
                else
                {
                    if (fS1 >= -fExtDet1)
                    {
                        if (fS1 <= fExtDet1)  // region 1 (side)
                        {
                            fS0 = mLength;
                            fTmpS1 = -(fA01 * fS0 + fB1);
                            if (fTmpS1 < -otherSegment.mLength)
                            {
                                fS1 = -otherSegment.mLength;
                                fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                    fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                            }
                            else if (fTmpS1 <= otherSegment.mLength)
                            {
                                fS1 = fTmpS1;
                                fSqrDist = -fS1 * fS1 + fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                            }
                            else
                            {
                                fS1 = otherSegment.mLength;
                                fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                    fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                            }
                        }
                        else  // region 2 (corner)
                        {
                            fS1 = otherSegment.mLength;
                            fTmpS0 = -(fA01 * fS1 + fB0);
                            if (fTmpS0 < -mLength)
                            {
                                fS0 = -mLength;
                                fSqrDist = fS0 * (fS0 - ((float)2.0) * fTmpS0) +
                                    fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                            }
                            else if (fTmpS0 <= mLength)
                            {
                                fS0 = fTmpS0;
                                fSqrDist = -fS0 * fS0 + fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                            }
                            else
                            {
                                fS0 = mLength;
                                fTmpS1 = -(fA01 * fS0 + fB1);
                                if (fTmpS1 < -otherSegment.mLength)
                                {
                                    fS1 = -otherSegment.mLength;
                                    fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                        fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                                }
                                else if (fTmpS1 <= otherSegment.mLength)
                                {
                                    fS1 = fTmpS1;
                                    fSqrDist = -fS1 * fS1 + fS0 * (fS0 + ((float)2.0) * fB0)
                                        + fC;
                                }
                                else
                                {
                                    fS1 = otherSegment.mLength;
                                    fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                        fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                                }
                            }
                        }
                    }
                    else  // region 8 (corner)
                    {
                        fS1 = -otherSegment.mLength;
                        fTmpS0 = -(fA01 * fS1 + fB0);
                        if (fTmpS0 < -mLength)
                        {
                            fS0 = -mLength;
                            fSqrDist = fS0 * (fS0 - ((float)2.0) * fTmpS0) +
                                fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                        }
                        else if (fTmpS0 <= mLength)
                        {
                            fS0 = fTmpS0;
                            fSqrDist = -fS0 * fS0 + fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                        }
                        else
                        {
                            fS0 = mLength;
                            fTmpS1 = -(fA01 * fS0 + fB1);
                            if (fTmpS1 > otherSegment.mLength)
                            {
                                fS1 = otherSegment.mLength;
                                fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                    fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                            }
                            else if (fTmpS1 >= -otherSegment.mLength)
                            {
                                fS1 = fTmpS1;
                                fSqrDist = -fS1 * fS1 + fS0 * (fS0 + ((float)2.0) * fB0)
                                    + fC;
                            }
                            else
                            {
                                fS1 = -otherSegment.mLength;
                                fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                    fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                            }
                        }
                    }
                }
            }
            else
            {
                if (fS1 >= -fExtDet1)
                {
                    if (fS1 <= fExtDet1)  // region 5 (side)
                    {
                        fS0 = -mLength;
                        fTmpS1 = -(fA01 * fS0 + fB1);
                        if (fTmpS1 < -otherSegment.mLength)
                        {
                            fS1 = -otherSegment.mLength;
                            fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                        }
                        else if (fTmpS1 <= otherSegment.mLength)
                        {
                            fS1 = fTmpS1;
                            fSqrDist = -fS1 * fS1 + fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                        }
                        else
                        {
                            fS1 = otherSegment.mLength;
                            fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                        }
                    }
                    else  // region 4 (corner)
                    {
                        fS1 = otherSegment.mLength;
                        fTmpS0 = -(fA01 * fS1 + fB0);
                        if (fTmpS0 > mLength)
                        {
                            fS0 = mLength;
                            fSqrDist = fS0 * (fS0 - ((float)2.0) * fTmpS0) +
                                fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                        }
                        else if (fTmpS0 >= -mLength)
                        {
                            fS0 = fTmpS0;
                            fSqrDist = -fS0 * fS0 + fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                        }
                        else
                        {
                            fS0 = -mLength;
                            fTmpS1 = -(fA01 * fS0 + fB1);
                            if (fTmpS1 < -otherSegment.mLength)
                            {
                                fS1 = -otherSegment.mLength;
                                fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                    fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                            }
                            else if (fTmpS1 <= otherSegment.mLength)
                            {
                                fS1 = fTmpS1;
                                fSqrDist = -fS1 * fS1 + fS0 * (fS0 + ((float)2.0) * fB0)
                                    + fC;
                            }
                            else
                            {
                                fS1 = otherSegment.mLength;
                                fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                    fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                            }
                        }
                    }
                }
                else   // region 6 (corner)
                {
                    fS1 = -otherSegment.mLength;
                    fTmpS0 = -(fA01 * fS1 + fB0);
                    if (fTmpS0 > mLength)
                    {
                        fS0 = mLength;
                        fSqrDist = fS0 * (fS0 - ((float)2.0) * fTmpS0) +
                            fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                    }
                    else if (fTmpS0 >= -mLength)
                    {
                        fS0 = fTmpS0;
                        fSqrDist = -fS0 * fS0 + fS1 * (fS1 + ((float)2.0) * fB1) + fC;
                    }
                    else
                    {
                        fS0 = -mLength;
                        fTmpS1 = -(fA01 * fS0 + fB1);
                        if (fTmpS1 < -otherSegment.mLength)
                        {
                            fS1 = -otherSegment.mLength;
                            fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                        }
                        else if (fTmpS1 <= otherSegment.mLength)
                        {
                            fS1 = fTmpS1;
                            fSqrDist = -fS1 * fS1 + fS0 * (fS0 + ((float)2.0) * fB0)
                                + fC;
                        }
                        else
                        {
                            fS1 = otherSegment.mLength;
                            fSqrDist = fS1 * (fS1 - ((float)2.0) * fTmpS1) +
                                fS0 * (fS0 + ((float)2.0) * fB0) + fC;
                        }
                    }
                }
            }
        }
        else
        {
            // The segments are parallel.  The average b0 term is designed to
            // ensure symmetry of the function.  That is, dist(seg0,seg1) and
            // dist(seg1,seg0) should produce the same number.
            float fE0pE1 = mLength + otherSegment.mLength;
            float fSign = (fA01 > (float)0.0 ? (float) - 1.0 : (float)1.0);
            float fB0Avr = ((float)0.5) * (fB0 - fSign * fB1);
            float fLambda = -fB0Avr;
            if (fLambda < -fE0pE1)
            {
                fLambda = -fE0pE1;
            }
            else if (fLambda > fE0pE1)
            {
                fLambda = fE0pE1;
            }

            fS1 = -fSign * fLambda * otherSegment.mLength / fE0pE1;
            fS0 = fLambda + fSign * fS1;
            fSqrDist = fLambda * (fLambda + ((float)2.0) * fB0Avr) + fC;
        }
        // we don't need the following stuff - it's for calculating closest point
        //    m_kClosestPoint0 = mOrigin + fS0*mDirection;
        //    m_kClosestPoint1 = otherSegment.mOrigin + fS1*otherSegment.mDirection;
        //    m_fSegment0Parameter = fS0;
        //    m_fSegment1Parameter = fS1;
        return Mathf.Abs(fSqrDist);
    }
}

